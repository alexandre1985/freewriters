<?php
//header('Content-Type: application/json;charset=utf-8');

try {

    // require authentication data
    require './credentials.php';
    $table = 'books';

    // Create connection
    $conn = new mysqli($server, $mysql_user, $mysql_pass, $database);
    
    // Check connection
    if($conn->connect_errno) {
        throw new Exception('Database connection failed: ' . $conn->connect_error, 1);
    }

    // find out how many rows are in the table
    if($result = $conn->query("SELECT active , COUNT(*) AS count FROM books")) {
        $data = $result->fetch_assoc();
        $result = $data['count'];
    }
    
} catch (Exception $e) {
    echo $e;

} finally {
    $conn->close();
    
    print $result;
}

